// BDD example:
// As a individual
// When I create a list
// Then I see list items available

// Hints and Tips
// You can use browser.pause(); to debug
// Note doesn't work with node 8 version
//
// Locators useful website: http://luxiyalu.com/protractor-locators-selectors/

// Helper Class
const EC = protractor.ExpectedConditions;

// Page Object
const header = element(by.id('header'));
const form = element(by.id('todo-form'));
const todosBinding = element(by.className('ng-binding'));

// Tests
describe('angularjs todo page', () => {
  // TODO: Test runs with "npm test" to debug issue

  beforeAll(() => {
    browser.get('http://todomvc.com/examples/angularjs_require/');
  });

  it('should use list items feature', () => {
    expect(header.getText()).toEqual('todos');

    form.sendKeys('List item 1');
    form.sendKeys(protractor.Key.ENTER);

    browser.wait(EC.visibilityOf(todosBinding), 5000);
    expect(todosBinding.getText()).toEqual('List item 1');
  });

  //TODO: What other tests do you think should be included
});
